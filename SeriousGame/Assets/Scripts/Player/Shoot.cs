﻿using Bullets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ViveHandTracking;

public class Shoot : MonoBehaviour
{
    private Transform _selection;
    private bool puedes_disparar = false;
    [SerializeField] private Image _redScreen;
    [SerializeField] private GameObject _bullet;
    [SerializeField] private Transform _bulletPosition;

    public ShakeCamera _shake;
    public bool _isRunning;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            //StartCoroutine(_shake.Shake(0.5f, 0.2f));
            StartCoroutine(redScreen(50));

        }
    }


    IEnumerator redScreen(float alpha)
    {
        Color32 originalColor = _redScreen.color;
        _isRunning = true;
        float elapsed = 0.0f;
        while (alpha >  0)
        {
            if (alpha >= 0)
                alpha -= elapsed;
            if (alpha < 0)
                alpha=0;
            _redScreen.gameObject.SetActive(true);
            _redScreen.color = new Color32(255,0, 0,(byte)alpha);
            elapsed += Time.deltaTime;
            //Debug.Log(elapsed+" "+alpha);
            yield return null;
        }
        _redScreen.gameObject.SetActive(false);
        _redScreen.color = originalColor;
        _isRunning = false;


    }
    //origninal
    public void ShootBullet(TextMesh letter)
    {
        _bullet.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = letter.text;
        if (puedes_disparar == true)
            InstantiateBullet(_selection);
    }
    public void InstantiateBullet(Transform enemyselected)
    {
        GameObject go;
        go = Instantiate(_bullet, _bulletPosition);
        //original 
        var bullets = go.GetComponent<InstanceBullet>();
        bullets.SetPosition(enemyselected);

        ///////////////////////////////////////////
        ///////////////RIGIDBODY//////////////////
        /////////////////////////////////////////
        /*var bullets = go.GetComponent<Bullet>().GetComponent<Rigidbody>();
        bullets.velocity = transform.TransformDirection(new Vector3(_selection.gameObject.transform.position.x,
            _selection.gameObject.transform.position.y,
            _selection.gameObject.transform.position.z));*/


        //prueba2
        //bullets.SetPosition(mouse);
    }
    private void Update()
    {
        //if (_selection != null)
        //{
        //    var _selectionRenderer = _selection.GetComponent<Renderer>();
        //    //Debug.Log(_selection.position);
        //    puedes_disparar = false;
        //}
        ////si quiere el mouse
        //var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //RaycastHit hit;
        //if (Physics.Raycast(ray, out hit))
        //{
        //    var selection = hit.transform;
        //    var selectionRenderer = selection.GetComponent<Renderer>();
        //    if (selection.gameObject.tag == "Enemy")
        //    {
        //        puedes_disparar = true;
               
        //    }
        //    _selection = selection;
        //}
    }
    //prueba2
    /*private void Update()
    {
        mouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane - Camera.main.nearClipPlane);
        mouse = Camera.main.ScreenToWorldPoint(mouse);
    }*/

    //prueba 1
    /*public void ShootBullet(TextMesh letter, Vector3 mousePos)
    {
        _bullet.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = letter.text;
        InstantiateBullet(mousePos);
    }
    private void InstantiateBullet(Vector3 mouseAtack)
    {
        GameObject go;
        go = Instantiate(_bullet, _bulletPosition);
        var bullets = go.GetComponent<InstanceBullet>();
        bullets.SetPosition(mouseAtack);

    }*/
    //prueba 2


    // Update is called once per frame
    //void Update()
    //{
    //    string str = "";
    //    string strPosition = "";
    //    if (GestureProvider.RightHand != null) { 
    //        for (int q = 0; q < GestureProvider.RightHand.points.Length; q++)
    //        {
    //            str += GestureProvider.RightHand.points[q].x.ToString("F") + ";";
    //            str += GestureProvider.RightHand.points[q].y.ToString("F") + ";";
    //            str += GestureProvider.RightHand.points[q].z.ToString("F") + ";";
    //        }

    //    }
    //    if (Input.GetKeyDown(KeyCode.A))
    //        Debug.Log("Puntos: " + str);
    //    if (Input.GetKeyDown(KeyCode.C)) { 
    //        Debug.Log("Posicion: " + GestureProvider.RightHand.position.ToString("F") + ";");
    //    }
    //    //foreach (var item in GestureProvider.LeftHand.position)
    //    //{
    //    //    Debug.Log(item);
    //    //}

    //}


}

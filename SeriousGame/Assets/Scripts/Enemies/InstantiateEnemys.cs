﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateEnemys : MonoBehaviour
{
    [SerializeField] private List<GameObject> _enemyPrefabList;
    [SerializeField] private Transform _target;
    [SerializeField] private Transform _enemyOutput;
    [SerializeField] private GameObject _reticle;

    private List<int> _enemiesId;

    public List<GameObject> enemys;
    public GameObject _bullet;

    public static float waitSeconds;
    public int _maxEnemiesInScreen;
    private int enemyLevel;

    [HideInInspector]
    public int _enemiesCounter;


    void Start()
    {

        //for (int i = 0; i < 5; i++)
        //{
        //    Vector3 position = new Vector3(Random.Range(-8.0f, 10.0f), -9.0f, Random.Range(-16.0f, 2.0f));
        //    InstantiateEnemies(position);
        //}
        _enemiesId = new List<int>();
        updateLevel();
        _enemiesCounter = 0;
        StartCoroutine(insantiateEnemies());

    }

    public static void updateLevel()
    {
            
        switch (Globals.currentLevel)
        {
            case 0:
            case 1:
            case 2:
                //Globals.enemiesQuantity = 7;
                waitSeconds = 5.0f;
                break;
            //case 1:
            //    Globals.enemiesQuantity = 10;
            //    waitSeconds = 4.0f;
            //    break;
            //case 2:
            //    Globals.enemiesQuantity = 15;
            //    waitSeconds = 5.0f;
            //    break;

            default:
                break;
             

        }

    }
    private string RandomString()
    {
        int num = 0;
        if (Globals.alter)
        {
            switch (Globals.levelRoom)
            {
                case 1:
                    num = Random.Range(0, 3);
                    break;
                case 2:
                    num = Random.Range(3, 6);
                    break;
                case 3:
                    num = Random.Range(6, 9);
                    break;
                case 4:
                    num = Random.Range(10, 13);
                    break;
                case 5:
                    num = Random.Range(13, 16);
                    break;
                case 6:
                    num = Random.Range(16, 19);
                    break;
                case 7:
                    num = Random.Range(19, 22);
                    break;
                case 8:
                    num = Random.Range(22, 25);
                    break;
                default:
                    break;
            }
        }
        else 
        {
            switch (Globals.levelRoom)
            {
                case 1:
                    num = Random.Range(0, 6);
                    break;
                case 2:
                    num = Random.Range(6, 13);
                    break;
                case 3:
                    num = Random.Range(13, 19);
                    break;
                case 4:
                    num = Random.Range(19, 25);
                    break;
                default:
                    break;
            }
        }
        
        if (num == 9)
            num += 1;
        string letter = ((char)('a' + num)).ToString();

        return letter;
    }
    public IEnumerator insantiateEnemies()
    {
        //for (int i = 0; i < Globals.enemiesQuantity; i++)
        while (true)
        {
            yield return new WaitForSeconds(2.5f);

            if (_enemiesCounter < _maxEnemiesInScreen) {
                Vector3 position = new Vector3(Random.Range(-8.0f, 10), -9.0f, Random.Range(6.5f, 8.0f));
                enemyLevel = Random.Range(0, Globals.currentLevel + 1);
                InstantiateEnemies(position, _enemyPrefabList[enemyLevel], RandomString());
                _enemiesCounter++;
            }

            //obteniendo el angulo para las flechas
            //Transform prueba = _target;
            //Globals.angle = Mathf.Atan((prueba.transform.position.y - position.y) / (prueba.transform.position.x -
            //    position.x));
            //Globals.angle = Globals.angle * Mathf.Rad2Deg;

            //Vector3 difference = _target.position - position;
            //Globals.rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        }

    }

    private void InstantiateEnemies(Vector3 position, GameObject _dificultPrefab, string letter)
    {
        GameObject go;
        go = Instantiate(_dificultPrefab, _enemyOutput);
        enemys.Add(go);
        var enemiesList = go.GetComponent<SetEnemyPosition>();
        enemiesList.SetPosition(position, _target, _bullet, letter, _reticle, gameObject);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using ViveHandTracking;

public class BossShoot : MonoBehaviour
{
    public GameObject _player;
    public GameObject _boss;
    public GameObject _correct;
    public GameObject _wrong;
    private Vector3 velocity = Vector3.zero;
    private string _text;
    int bossLife = 24;
    int playerLife = 5;
    int correct = 0;
    int wrong = 0;
    int contLetters = 0;
    bool conter;
    private Animator m_animator;
    AudioSource _audio;
    private string _letter = "";

    // Start is called before the first frame update
    void Start()
    {
        conter = false;
        _audio = _boss.GetComponent<AudioSource>();
        m_animator = _boss.transform.GetChild(0).gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (Globals.letter)
        {
            case 0: _letter = "A"; break;
            case 1: _letter = "B"; break;
            case 2: _letter = "C"; break;
            case 3: _letter = "D"; break;
            case 4: _letter = "E"; break;
            case 5: _letter = "F"; break;
            case 6: _letter = "G"; break;
            case 7: _letter = "H"; break;
            case 8: _letter = "I"; break;
            case 9: _letter = "K"; break;
            case 10: _letter = "L"; break;
            case 11: _letter = "M"; break;
            case 12: _letter = "N"; break;
            case 13: _letter = "O"; break;
            case 14: _letter = "P"; break;
            case 15: _letter = "Q"; break;
            case 16: _letter = "R"; break;
            case 17: _letter = "S"; break;
            case 18: _letter = "T"; break;
            case 19: _letter = "U"; break;
            case 20: _letter = "V"; break;
            case 21: _letter = "W"; break;
            case 22: _letter = "X"; break;
            case 23: _letter = "Y"; break;
            default: _letter = ""; break;

        }
        

        if (Globals.bossAttacking)
        {
            //SIN HTC
            //if (Input.GetKey(_text))
            //{
            //    _letter = "";
            //    Globals.letter = -1;
            //    //transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = "GOOD";
            //    conter = true;
            //}
            //CON HTC
            if ((Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch")) && _letter == (_text) && GestureProvider.RightHand != null)
            {
                _letter = "";
                Globals.letter = -1;
                conter = true;
            }


            if (conter)
                transform.position = Vector3.MoveTowards(transform.position, _boss.transform.position, Time.deltaTime *  10f);
            else
                transform.position = Vector3.MoveTowards(transform.position, _player.transform.position, Time.deltaTime * 0.5f);
        }


    }

    public void BulletMove(int level){
        _text = gameObject.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text;
        conter = false;
        Globals.levelRoom = level;
    }

    public void OnCollisionEnter(Collision collision)
    {
        conter = false;
        contLetters++;
        if (collision.collider.tag == "Enemy")
        {
            m_animator.Play("Hit",0,0.5f);
            _audio.Play();
            correct++;
            _correct.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = correct.ToString();
        }
        else if (collision.collider.tag == "Player")
        {
            wrong++;
            _wrong.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = wrong.ToString();
        }

        transform.position = new Vector3(-11.15f, -20.7f, 0.02f);
        //gameObject.SetActive(false);
        Globals.bossAttacking = false;

        if(contLetters == 24)
        {
            Debug.Log("correct: " + correct);
            Debug.Log("wrong: " + wrong);
            Globals.finalBoss = false;
            SceneManager.LoadScene("FinalGame");
        }
    }
}

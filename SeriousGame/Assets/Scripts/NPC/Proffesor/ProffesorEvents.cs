﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProffesorEvents : MonoBehaviour
{
    private Animator animator;
    public DialogTrigger dialog;

    private int moveAnimation = Animator.StringToHash("ProffesorMovement");
    private int talkAnimation = Animator.StringToHash("Talk");
    private bool dialogueStarted=false;

    void Start()
    {
        animator=GetComponent<Animator>();

        if(SceneManager.GetActiveScene().name== "MainMenu")
            animator.SetTrigger(moveAnimation);

    }
    private void OnMouseDown()
    {
        if(SceneManager.GetActiveScene().name == "LevelSelector"&& Input.GetMouseButtonDown(0))
        {
            Debug.Log("batalla final");
        }
    }
    void Update()
    {
        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("ProffesorMovement") && !dialogueStarted)
        {
            StartCoroutine(StartDialogue());
        }
    }

    IEnumerator StartDialogue()
    {

        dialog.TriggerDialogue();
        dialogueStarted = true;
        while (true)
        {
            if (dialog.SentenceFinish() && (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch")))
            {
                dialog.GoToNextSentence();

                if (dialog.EndDialogue())
                {
                    SceneManager.LoadScene("LevelSelector");
                    yield return false;
                }
            }
            yield return null;
        }
      
    }

    public bool startDialogue()
    {
        dialog.TriggerDialogue();

        if (dialog.EndDialogue())
            return false;

        return true;
    }
}

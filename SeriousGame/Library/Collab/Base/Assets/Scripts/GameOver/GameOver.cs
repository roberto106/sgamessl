﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private TextMeshProUGUI _maxScoreText;
    [SerializeField] private TextMeshProUGUI _currentScoreText;

    int _currentScore;
    int _maxScore;
    void Start()
    {
        _currentScore = Globals.playerScore;
        _maxScore = PlayerPrefs.GetInt("MaxScore");
        if (_currentScore > _maxScore)
            PlayerPrefs.SetInt("MaxScore", _currentScore);

        _maxScoreText.text = "Puntaje máximo: " + PlayerPrefs.GetInt("MaxScore");
        _currentScoreText.text = "Puntaje: " + _currentScore.ToString();

        Globals.bossesBeaten = 0;
        Globals.playerPoints = 0;
        Globals.playerScore = 0;
        Globals.playerLive = 15;
        Globals.currentLevel = 0;
        Globals.enemiesQuantity = 10;



        StartCoroutine(changeScene());
    }

    IEnumerator changeScene()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            SceneManager.LoadSceneAsync("Difficulty");
            yield break;
        }

    }
}

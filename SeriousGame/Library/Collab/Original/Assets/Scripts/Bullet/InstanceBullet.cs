﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace Bullets
{ 
    public class InstanceBullet : MonoBehaviour
    {
        private NavMeshAgent _angent;
        //original
        private Transform _target;
        // Start is called before the first frame update
        //original
        public void SetPosition(Transform enemyPosition)
        {
            _angent = GetComponent<NavMeshAgent>();
            _target = enemyPosition;
        }

        public void Update()
        {
            if (GetComponent<NavMeshAgent>().enabled)
                _angent.SetDestination(_target.position);
        }
    }
}
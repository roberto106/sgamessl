﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace Bullets
{ 
    public class InstanceBullet : MonoBehaviour
    {
        private NavMeshAgent _angent;
        //original
        private Transform _target;
        private Vector3 velocity = Vector3.zero;
        // Start is called before the first frame update
        //original
        public void SetPosition(Transform enemyPosition)
        {
            //this.gameObject.SetActive(true);
            //_angent = GetComponent<NavMeshAgent>();
            _target = enemyPosition;
        }

        public void Update()
        {
            /*if (GetComponent<NavMeshAgent>().enabled)
                _angent.SetDestination(_target.position);*/
            this.gameObject.transform.position = Vector3.SmoothDamp(this.gameObject.transform.position, _target.position,
                ref velocity, 0.3f);
        }
    }
}
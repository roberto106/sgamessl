﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    NavMeshAgent _bullet;
    public GameObject cubo;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
        
        _bullet = GetComponent<NavMeshAgent>();
        GetComponent<NavMeshAgent>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 5.0f,
            player.transform.position.z);
        if (Globals.disparo)
            GetComponent<NavMeshAgent>().enabled = true;
        else
            GetComponent<NavMeshAgent>().enabled = false;
    }
    public void figth(Transform destino)
    {
        if(GetComponent<NavMeshAgent>().enabled == true)
        {
            //_bullet.destination = cubo.transform.position;
            _bullet.destination = destino.position;
        }

    }

}

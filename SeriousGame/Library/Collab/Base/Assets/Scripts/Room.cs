﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Room : MonoBehaviour
{

    private bool _isSelected;
    private int numRoom;
    public GameObject reticle;
    private GameObject profe;
    // Start is called before the first frame update
    void Start()
    {
        profe = GameObject.Find("Profesor");
        numRoom = int.Parse(this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<TextMesh>().text);
    }

    void OnTriggerEnter(Collider coll)
    {
        if (Input.GetButton("LViveConTrackpadPress") || Input.GetButton("RViveConTrackpadPress"))
        {
            Globals.levelRoom = numRoom;
            SceneManager.LoadScene("Demo");
        }
    }

    void OnCollisionEnter(Collision coll)
    {
        if (Input.GetButton("LViveConTrackpadPress") || Input.GetButton("RViveConTrackpadPress"))
        {
            Globals.levelRoom = numRoom;
            SceneManager.LoadScene("Demo");
        }
    }

    // Update is called once per frame
    void Update()
    {
        bool dialogueStarted = profe.GetComponent<ProffesorFinalLevel>().dialogueStarted;
        bool dialogueEnded = profe.GetComponent<ProffesorFinalLevel>().dialogueEnded;
        if (reticle != null)
        {
            var id = reticle.GetComponent<ReticlePoser>().hitTarget.GetInstanceID();
            if (id == gameObject.GetInstanceID() && !dialogueEnded && !dialogueStarted)
            {
                if (this.gameObject.GetComponent<SkinnedMeshRenderer>().materials != null)
                    foreach (var item in this.gameObject.GetComponent<SkinnedMeshRenderer>().materials)
                    {
                        item.color = Color.grey;
                    }
                _isSelected = true;
            } else
            {
                foreach (var item in this.gameObject.GetComponent<SkinnedMeshRenderer>().materials)
                {
                    item.color = Color.white;
                }
                _isSelected = false;
            }
        }
        if ((Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch")) && _isSelected)
        {
            Globals.levelRoom = numRoom;
            SceneManager.LoadScene("Demo");
        }
    }

    // private void OnMouseOver()
    // {
    //     if (this.gameObject.GetComponent<SkinnedMeshRenderer>().materials != null)
    //         foreach (var item in this.gameObject.GetComponent<SkinnedMeshRenderer>().materials)
    //         {
    //             item.color = Color.grey;
    //         }
    //     _isSelected = true;
    // }

    // private void OnMouseExit()
    // {
    //     foreach (var item in this.gameObject.GetComponent<SkinnedMeshRenderer>().materials)
    //     {
    //         item.color = Color.white;
    //     }
    //     _isSelected = false;
    // }

    void OnMouseDown()
    {
        // load a new scene
        Globals.levelRoom = numRoom;
        SceneManager.LoadScene("Demo");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ViveHandTracking;

public class DialogManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _npcName; 
    [SerializeField] private TextMeshProUGUI _npcSpeech;
    // Start is called before the first frame update
    int k;
    GestureResultRaw pointsResult;
    GestureResult gestureResult; 
    public void Awake()
    {
        k = 0;
        pointsResult = new GestureResultRaw();
        gestureResult = new GestureResult(pointsResult);
    }
    public void SetData(string npcName,string text)
    {
        _npcName.text = npcName;
        _npcSpeech.text = text;
    }
    public void Update()
    {
        if (k >= 100 && k <= 101) {
            _npcSpeech.text = k.ToString();
            _npcSpeech.text = gestureResult.points.GetValue(0,0,0).ToString();
        }
            k++;
    }
}

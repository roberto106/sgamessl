﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Level;

public class ProffesorFinalLevel : MonoBehaviour
{
    [SerializeField] private GameObject level;
    [SerializeField] private GameObject levelAlter;
    [SerializeField] private GameObject correct;
    [SerializeField] private GameObject wrong;
    public DialogTrigger dialog;
    public bool dialogueStarted = false;
    public bool dialogueEnded = false;
    public GameObject _reticle;

    private bool finalfight;

    private void Start()
    {
        finalfight = false;
        gameObject.SetActive(false);
        if (Globals.levelUnlocked == Globals.cantLevel + 1)
        {
            gameObject.SetActive(true);
        }
    }
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
            StartCoroutine(StartDialogue());
    }

    private void ViveControllSelect()
    {
        if (_reticle != null)
        {
            var id = _reticle.GetComponent<ReticlePoser>().hitTarget.GetInstanceID();
            if (id == gameObject.GetInstanceID())
            {
                if (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch"))
                {
                    finalfight = true;
                    StartCoroutine(StartDialogue());
                }
            }
        }
    }

    IEnumerator StartDialogue()
    {

        dialog.TriggerDialogue();
        dialogueStarted = true;
        while (true)
        {
            if (dialog.SentenceFinish() && (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch")))
            {
                dialog.GoToNextSentence();

                if (dialog.EndDialogue())
                {
                    dialogueEnded = true;
                    yield return false;
                }
            }
            yield return null;
        }

    }
    private void Update()
    {
        if (!finalfight)
        {
            ViveControllSelect();
        }
        if (dialogueEnded)
        {

            if (Input.GetButton("LViveConTrackpadPress") || Input.GetButton("RViveConTrackpadPress"))
            {
                Debug.Log("cerrar");
                dialog.ClosePanel();
                dialogueEnded = false;
                dialogueStarted = false;
            }
            else if (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch"))
            {
                gameObject.SetActive(false);
                //level.SetActive(false);
                correct.SetActive(true);
                wrong.SetActive(true);
                dialog.ClosePanel();
                if (Globals.alter)
                {
                    levelAlter.GetComponent<LevelSelector>().ActivateFinalLevel();
                }
                else
                {
                    level.GetComponent<LevelSelector>().ActivateFinalLevel();
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    public Dialog dialogue;
    
    public void TriggerDialogue()
    {
        FindObjectOfType<DialogSystem>().StartDialogue(dialogue);
    }
    public bool EndDialogue()
    {
        return FindObjectOfType<DialogSystem>().EndDialogue();
    }

    public bool SentenceFinish()
    {
        return FindObjectOfType<DialogSystem>().FinishSentence();
    }
    public void GoToNextSentence()
    {
        FindObjectOfType<DialogSystem>().DisplayNextSentence();
    }
    public void ClosePanel()
    {
        FindObjectOfType<DialogSystem>().ClosePanel();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class classRoom : MonoBehaviour
{
    
    private bool _isSelected;
    private int numRoom;

    // Start is called before the first frame update
    void Start()
    {
        numRoom = int.Parse(this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<TextMesh>().text);
    }

    void OnTriggerEnter(Collider coll)
    {
        Debug.Log("trigger");
        Debug.Log(coll.name);
    }

    void OnCollisionEnter(Collision coll)
    {
        Debug.Log("collision");
        Debug.Log(coll.collider.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("casa");
        }
    }

    private void OnMouseOver()
    {
        if (this.gameObject.GetComponent<SkinnedMeshRenderer>().materials != null)
            foreach (var item in this.gameObject.GetComponent<SkinnedMeshRenderer>().materials)
            {
                item.color = Color.grey;
            }
        Debug.Log("casa");
        _isSelected = true;
    }

    private void OnMouseExit()
    {
        foreach (var item in this.gameObject.GetComponent<SkinnedMeshRenderer>().materials)
        {
            item.color = Color.white;
        }
        _isSelected = false;
    }

    void OnMouseDown()
    {
        // load a new scene
        Globals.levelRoom = numRoom;
        SceneManager.LoadScene("Demo");
    }
}

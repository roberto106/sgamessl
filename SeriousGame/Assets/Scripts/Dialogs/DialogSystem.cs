﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogSystem : MonoBehaviour
{
    public GameObject panel;

    private Queue<string> sentences;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;

    public bool finishSentence = false;
    public bool finishDialogue = false;

    void Start()
    {
        sentences = new Queue<string>();
    }
    
    public void StartDialogue(Dialog dialogue)
    {
        panel.SetActive(true);
        nameText.text = dialogue.name;
        sentences.Clear();
        foreach (var sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }
    public void ClosePanel()
    {
        panel.SetActive(false);
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            finishDialogue=true;
            return;
        }
        finishSentence = false;
        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    public bool EndDialogue()
    {
        Debug.Log("Conversacion finalizada");
        return finishDialogue;
    }

    public bool FinishSentence()
    {
        return finishSentence;
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        int lettercount = 0;
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            lettercount++;
            if (lettercount == sentence.ToCharArray().Length)
                finishSentence = true;
            yield return null;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Menu : MonoBehaviour
{
    [SerializeField] private string _sceneName;
    [SerializeField] private TextMesh _text;
    private bool isInitialized = false;
    public void Update()
    {

        if (Input.anyKeyDown && !isInitialized)
        {

            StartCoroutine(Counter(0.025f, 3));
            isInitialized = true;

        }
    }

    IEnumerator Counter(float duration, int number)
    {
        Vector3 originalSize = _text.transform.localScale;
        Vector3 vec3 = originalSize;
        float elapsed = 0.0f;
        while (number > 0)
        {
            _text.text = number.ToString();
            if (vec3.x < 0)
            {
                number--;
                _text.transform.localScale = originalSize;
                vec3 = originalSize;
            }

            vec3.x -= duration;
            vec3.y -= duration;
            _text.transform.localScale = vec3;
            yield return null;

        }
        _text.text = "Empieza";
        isInitialized = false;
        Play();

    }

    public void Play()
    {
        SceneManager.LoadScene(_sceneName);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [SerializeField] private GameObject _bullet;
    [SerializeField] private GameObject _boss;

    //string[] listWords = new string[]{ "bien", "tiempo", "cueva", "wisky", "lupa", "hada", "agua", "fenix", "res", "queso"};
    List<string> listLetters = new List<string>();
    Dictionary<string, int> diccLetter = new Dictionary<string, int>();

    void fillList()
    {
        for (int i = 0; i < 25 ; i++)
        {
            if (i != 9)
            {
                listLetters.Add(((char)('a' + i)).ToString());
            }
        }
        for (int i = 0; i < listLetters.Count; i++)
        {
            int level = (int)(Mathf.Floor(i /3)) + 1;
            diccLetter.Add(listLetters[i], level);
        }
        Shuffle();
        //for (int i = 0; i < 24; i++)
        //{
        //    Debug.Log(listLetters[i]);
        //}
    }

    public void Shuffle()
    {
        int n = listLetters.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            var value = listLetters[k];
            listLetters[k] = listLetters[n];
            listLetters[n] = value;
        }
    }

    //string[] listWords = new string[]{"abcd"};

    int cont = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(shooting());
        fillList();
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator shooting()
    {
        while (cont < 24)
        {
            yield return new WaitForSeconds(2);
            if(!Globals.bossAttacking)
            {
                _bullet.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = listLetters[cont].ToUpper();
                _bullet.transform.position = new Vector3(_boss.transform.position.x + 4.0f, _boss.transform.position.y, _boss.transform.position.z);
                Globals.bossAttacking = true;
                var _shoot = _bullet.GetComponent<BossShoot>();
                _shoot.BulletMove(diccLetter[listLetters[cont]]);
                cont++;
            }
            
        }
        
    }
}

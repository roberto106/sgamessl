﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Level;

public class ProffesorFinalLevel : MonoBehaviour
{
    [SerializeField] private GameObject level;
    public DialogTrigger dialog;
    private bool dialogueStarted = false;
    private bool dialogueEnded = false;

    private void Start()
    {
        Debug.Log("entro");
        gameObject.SetActive(false);
        if (Globals.levelUnlocked == 7)
            gameObject.SetActive(true);
    }
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
            StartCoroutine(StartDialogue());
    }
    
    IEnumerator StartDialogue()
    {

        dialog.TriggerDialogue();
        dialogueStarted = true;
        while (true)
        {
            if (dialog.SentenceFinish() && Input.GetKeyDown(KeyCode.A))
            {
                dialog.GoToNextSentence();

                if (dialog.EndDialogue())
                {
                    dialogueEnded = true;
                    yield return false;
                }
            }
            yield return null;
        }

    }
    private void Update()
    {
        if (dialogueEnded)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                gameObject.SetActive(false);
                dialog.ClosePanel();
                level.GetComponent<LevelSelector>().ActivateFinalLevel();
            }
            else if (Input.GetKeyDown(KeyCode.N))
            {
                dialog.ClosePanel();
                dialogueEnded = false;
            }
        }
    }
}

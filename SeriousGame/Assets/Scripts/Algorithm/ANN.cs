﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TensorFlow;
using System.IO;
using System.Globalization;
using System.Linq;
using System;
using ViveHandTracking;

public class ANN : MonoBehaviour
{
    List<float[]> dataset = new List<float[]>();

    private string path = Application.streamingAssetsPath;
    private int min = 0;
    private int max =99;

    enum Models
    {
       model3b,
       models2
    };
    enum Layers
    {
        dense_3,
        dense_4,
        dense_5,
    };
    enum Class
    {
        a=0,
        e=4,
        m=11,
        n=12,
        s=17
    };
    private void GetLetters()
    {
        if (Globals.alter)
        {
            min = (Globals.levelRoom - 1) * 3;
            max = Globals.levelRoom * 3;

        }
        else
        {
            min = (Globals.levelRoom - 1) * 6;
            max = Globals.levelRoom * 6;
        }
    }
    public void ReadCsv()
    {
        string filePath = path + "/test.csv";

        var contents = File.ReadAllText(filePath).Split('\n');

        var csv = from line in contents
                  select line.Split(',').ToArray();

        int headerRows = 1;


        foreach (var row in csv.Skip(headerRows).TakeWhile(r => r.Length > 1 && r.Last().Trim().Length > 0))
        {
            float[] col = new float[row.Length];

            for (int i = 0; i < row.Length; i++)
            {
                col[i] = float.Parse(row[i], CultureInfo.InvariantCulture);
            }
            dataset.Add(col);
        }

    }

    void StartNN(List<float>points,Models model, Layers layer)
    {

        float[,] positions = new float[1, 60];

        for (int i = 0; i < 60; i++)
        {
            positions[0, i] = points[i];
        }
        GetLetters();
        using (var graph = new TFGraph())
        {

            var modelFile = File.ReadAllBytes(path+"/"+model+".pb");


            graph.Import(modelFile);

            var session = new TFSession(graph);
            var runner = session.GetRunner();
            runner.AddInput(graph["dense_input"][0], positions);

            runner.Fetch(graph[layer+"/Softmax"][0]);

            var output = runner.Run();

            TFTensor result = output[0];
            bool jagged = true;

            var bestIdx = 0;
            float p = 0, best = 0;

            if (jagged)
            {
                var probabilities = ((float[][])result.GetValue(jagged: true))[0];

                Debug.Log("max: " + max);
                Debug.Log("min: " + min);
                for (int i = min; i < max; i++)
                {
                    if (probabilities[i] > best)
                    {
                        bestIdx = i;
                        best = probabilities[i];
                    }
                }

                //a, e, m, n, s
                if (model == Models.model3b)
                {
                    //if (bestIdx == 0 || bestIdx == 4 || bestIdx == 11 || bestIdx == 12 || bestIdx == 17)
                    //{
                    //    StartNN(points, Models.models2, Layers.dense_4);
                    //}
                    //else
                    //{
                        Globals.letter = bestIdx;
                        Debug.Log(bestIdx);

                    //}
                }
                else if (model == Models.models2) { 
                    Class labelClass= 0;
                    var lb = labelClass.GetType();
                    bestIdx = (int)lb.GetEnumValues().GetValue(bestIdx);
                    Globals.letter = bestIdx;
                    Debug.Log(bestIdx);

                }

            }
            else
            {
                var val = (float[,])result.GetValue(jagged: false);

                for (int i = min; i < max; i++)
                {
                    if (val[0, i] > best)
                    {
                        bestIdx = i;
                        best = val[0, i];
                    }
                }
                Globals.letter = bestIdx;
            }
        }

    }


    void Update()
    {
        if (Input.anyKeyDown && GestureProvider.RightHand != null)
        {
            List<float> points = new List<float>();

            float posx = GestureProvider.RightHand.points[0].x;
            float posy = GestureProvider.RightHand.points[0].y;
            float posz = GestureProvider.RightHand.points[0].z;
            for (int i = 1; i < GestureProvider.RightHand.points.Length; i++)
            {
                points.Add(GestureProvider.RightHand.points[i].x - posx);
                points.Add(GestureProvider.RightHand.points[i].y - posy);
                points.Add(GestureProvider.RightHand.points[i].z - posz);

            }

            StartNN(points,Models.model3b,Layers.dense_4);

        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using ViveHandTracking;

public class SetEnemyPosition : MonoBehaviour
{
    //public Rigidbody atack;
    //public GameObject Movbullet;
    [SerializeField] private GameObject _enemyBody;
    [SerializeField] private GameObject _letterCube;
    [SerializeField] private TextMesh _letter;
    [SerializeField] private ParticleSystem _particles;
    [SerializeField] private ParticleSystem _hitParticles;
    [SerializeField] private int _livePoints;
    [SerializeField] private GameObject _hitAnimation;
    [SerializeField] private GameObject _letterBurn;
    [SerializeField] private float _timeBetween;
    [SerializeField] private GameObject _materialColor;
    [SerializeField] private float _enemySpeed;

    private GameObject _reticle;

    private NavMeshAgent _angent;
    private Transform _target;
    private GameObject _helpPanelObject;
    private int _playerScore;
    private bool _isSelected;
    private float _speed;
    private float totalDuration;
    private bool _hit;
    private bool enemyHit;
    private Animator m_animator;
    AudioSource _audio;
    private string letter = "";
    void Start()
    {
        _playerScore = _livePoints;
        _angent = GetComponent<NavMeshAgent>();
        _angent.speed = _enemySpeed;
        GetComponent<NavMeshAgent>().enabled = false;
        // _enemyBody.GetComponent<SpriteRenderer>().enabled = false;
        _helpPanelObject = GameObject.Find("HelpCanvas");

        totalDuration = _particles.main.duration + _particles.main.startLifetimeMultiplier;
        StartCoroutine(waitPartices());
        Destroy(_particles, totalDuration);

        m_animator = _enemyBody.transform.GetChild(0).gameObject.GetComponent<Animator>();
        enemyHit = false;
        _audio = _enemyBody.GetComponent<AudioSource>();
        _speed = _angent.speed;

        if (Globals.diffFactor == 2)
            _angent.speed = 0.5f;
        if (Globals.diffFactor == 1.5)
            _angent.speed = 1.0f;
        if (Globals.diffFactor == 1)
            _angent.speed = 1.5f;
    }

    public void SetPosition(Vector3 position, Transform target, GameObject bullet, string letter, GameObject reticle)
    {
        _target = target;
        transform.position = position;
        _letter.text = letter.ToUpper();
        _reticle = reticle;
        //Movbullet = bullet;
        //Globals.enemyvivo = bicho_vivo;
        Transform prueba = _target;
        Globals.angle = Mathf.Atan((prueba.transform.position.y - transform.position.y) / (prueba.transform.position.x -
            transform.position.x));
        Globals.angle = Globals.angle * Mathf.Rad2Deg;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (_helpPanelObject.GetComponent<PlayerHelpData>()._playerlifePoints <= 1)
        {
            Globals.playerScore = _helpPanelObject.GetComponent<PlayerHelpData>()._playerScore;
            SceneManager.LoadScene("GameOver");
        }

        if (collision.collider.tag == "bullet")
        {
            Destroy(collision.gameObject);
            _hit = false;
            if (_hitParticles != null)
                _hitParticles.Play();
            SubstractLifePoints();
        }
        if (collision.gameObject.transform == _target)
        {
            _helpPanelObject.GetComponent<PlayerHelpData>()._playerlifePoints -= 1;
            _hit = true;
            _helpPanelObject.GetComponent<PlayerHelpData>()._playerLife.text = "Vida: " + _helpPanelObject.GetComponent<PlayerHelpData>()._playerlifePoints;
            //_livePoints = 0;
            SubstractLifePoints();
            Globals.minDistEnemy = Mathf.Infinity;
        }


    }
    private void OnMouseOver()
    {
        if (_materialColor.GetComponent<SkinnedMeshRenderer>().materials != null)
            foreach (var item in _materialColor.GetComponent<SkinnedMeshRenderer>().materials)
            {
                item.color = Color.grey;
            }
        // GetComponent<SpriteRenderer>().color = Color.gray;
        //Debug.Log("Selected");
        Globals.enemySelected = gameObject;
        _isSelected = true;
    }

    private void ViveControllSelect()
    {
        if (_reticle != null)
        {
            var id = _reticle.GetComponent<ReticlePoser>().hitTarget.GetInstanceID();
            if (id == gameObject.GetInstanceID())
            {
                if (_materialColor.GetComponent<SkinnedMeshRenderer>().materials != null)
                    foreach (var item in _materialColor.GetComponent<SkinnedMeshRenderer>().materials)
                    {
                        item.color = Color.grey;
                    }
                Globals.enemySelected = gameObject;
                _isSelected = true;
            }
            else
            {
                foreach (var item in _materialColor.GetComponent<SkinnedMeshRenderer>().materials)
                {
                    item.color = Color.white;
                }
                _isSelected = false;
                Globals.enemySelected = null;
            }
        }
    }

    private void OnMouseExit()
    {
        foreach (var item in _materialColor.GetComponent<SkinnedMeshRenderer>().materials)
        {
            item.color = Color.white;
        }
        // GetComponent<SpriteRenderer>().color = Color.white;
        _isSelected = false;
        Globals.enemySelected = null;
    }

    void Update()
    {

        //if (GestureProvider.RightHand != null)    
        //Debug.Log("Right gesture is " + GestureProvider.RightHand.gesture.GetType());
        ViveControllSelect();

        if (GetComponent<NavMeshAgent>().enabled)
            _angent.SetDestination(_target.position);

        if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            Globals.minDistEnemy = Mathf.Infinity;

        float enemyDist = Vector3.Distance(transform.position, _target.position);

        if(Globals.minDistEnemy > enemyDist)
            Globals.minDistEnemy = enemyDist;

        // if(Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch"))
        //     Debug.Log("Trigger press");

        // SELECTED ENEMY
        // if (_isSelected && Input.GetKeyDown(_letter.text.ToLower()) && _particles == null)
        // {
        //     if(_hitParticles!=null)
        //         _hitParticles.Play();
        //     SubstractLifePoints();
        // }

        //NEAREST ENEMY WITH GESTURE
        // if ((GestureProvider.RightHand.gesture == GestureType.Fist) && _particles == null && (Globals.minDistEnemy == enemyDist))
        // {
        //     _hit = false;
        //     if(_hitParticles!=null)
        //         _hitParticles.Play();
        //     SubstractLifePoints();

        // }

        //NEAREST ENEMY WITH KEY
        switch (Globals.letter)
        {
            case 0: letter = "A"; break;
            case 1: letter = "B"; break;
            case 2: letter = "C"; break;
            case 3: letter = "D"; break;
            case 4: letter = "E"; break;
            case 5: letter = "F"; break;
            case 6: letter = "G"; break;
            case 7: letter = "H"; break;
            case 8: letter = "I"; break;
            case 9: letter = "K"; break;
            case 10: letter = "L"; break;
            case 11: letter = "M"; break;
            case 12: letter = "N"; break;//
            case 13: letter = "O"; break;
            case 14: letter = "P"; break;
            case 15: letter = "Q"; break;
            case 16: letter = "R"; break;
            case 17: letter = "S"; break;//
            case 18: letter = "T"; break;
            case 19: letter = "U"; break;
            case 20: letter = "V"; break;//
            case 21: letter = "W"; break;
            case 22: letter = "X"; break;//
            case 23: letter = "Y"; break;
            default: letter = ""; break;

        }
        //Debug.Log(letter);
        //HTC al mas cercano
        //if ((Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch")) && letter.ToLower() == _letter.text.ToLower() && _particles == null && (Globals.minDistEnemy == enemyDist))
        //{
        //    letter = "";
        //    Globals.letter = -1;
        //    _hit = false;
        //    if (_hitParticles != null)
        //        _hitParticles.Play();
        //    SubstractLifePoints();

        //}
        //HTC apuntando
        if ((Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch")) && letter.ToLower() == _letter.text.ToLower() && _particles == null && _isSelected)
        {
            letter = "";
            Globals.letter = -1;
            _hit = false;
            _angent.speed = 0;
            if (_hitParticles != null)
                _hitParticles.Play();
            SubstractLifePoints();

        }
        //Sin HTC
        //if (_isSelected && Input.GetKeyDown(_letter.text.ToLower()) && _particles == null && !m_animator.GetCurrentAnimatorStateInfo(0).IsName("Hit"))
        //{
        //   //bullet.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = _letter.text;
        //   //bullet.SetActive(true);
        //   //bullet.GetComponent<MovProyectil>().Mover(transform.position);
        //   //Globals.disparo = true;
        //   //Movbullet.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = _letter.text;
        //   //Movbullet.SetActive(true);
        //   //Movbullet.GetComponent<Bullet>().figth(this.transform);
        //   //letter = "";
        //   //Globals.letter = -1;
        //   //original

        //   var _shoot=GameObject.Find("FPSController").GetComponent<Shoot>();
        //   _shoot.ShootBullet(_letter);

        //   //prueba 1
        //   //Vector3 posMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane - Camera.main.nearClipPlane);
        //   //Vector3 _posMouse = Camera.main.ScreenToWorldPoint(posMouse);
        //   //_posMouse = new Vector3(_posMouse.x, _posMouse.y, Camera.main.farClipPlane);
        //   //_shoot.ShootBullet(_letter, _posMouse);
        //   /*Vector3 posMouse = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
        //       Input.mousePosition.y, Input.mousePosition.z));
        //   _shoot.ShootBullet(_letter, posMouse);
        //   Debug.Log("x: " + posMouse.x);
        //   Debug.Log("y: " + posMouse.y);
        //   Debug.Log("z: " + posMouse.z);*/
        //   //prueba 2

        //   // _hit = false;
        //   // if(_hitParticles!=null)
        //   //    _hitParticles.Play();
        //   // SubstractLifePoints();

        //}


    }

    void SubstractLifePoints()
    {
        _livePoints -= 1;
        if(_hit){
            Globals.minDistEnemy = Mathf.Infinity;
            //test
            //Rigidbody atackInstance;
            //atackInstance = Instantiate(atack, this.transform.position, Quaternion.identity);
            //atackInstance.AddForce(this.transform.forward * 100 * 30);
            //
            Destroy(_enemyBody);
            //AddPlayerPoints();
            //Globals.enemyvivo = false;
        } else {
            _hitAnimation.SetActive(true);
            m_animator.Play("Hit",0,_timeBetween);
            _audio.Play();
            _angent.SetDestination(_enemyBody.transform.position);
            if(_livePoints <= 0)
            {
                _letterBurn.SetActive(true);
            }
            StartCoroutine(beHit());
        }
    }
    IEnumerator beHit()
    {
        yield return new WaitForSeconds(0.5f);
        _hitAnimation.SetActive(false);
        _angent.speed = _speed;
        if (_livePoints <= 0) {
            Globals.minDistEnemy = Mathf.Infinity;
            Destroy(_enemyBody);
            // if(!_hit)
            //     Globals.playerScore += _playerScore;
            _helpPanelObject.GetComponent<PlayerHelpData>()._playerScore += _playerScore;
            AddPlayerPoints();
        }
    }
    IEnumerator Disparo()
    {
        yield return new WaitForSeconds(0.5f);
        if(_livePoints <= 0)
        {
            Globals.minDistEnemy = Mathf.Infinity;
            Destroy(_enemyBody);
            Globals.playerScore += _playerScore;
            AddPlayerPoints();
        }
    }
    void AddPlayerPoints()
    {
        _helpPanelObject.GetComponent<PlayerHelpData>()._score.text = "Puntaje: " + (_helpPanelObject.GetComponent<PlayerHelpData>()._playerScore).ToString();

        //if (_helpPanelObject.GetComponent<PlayerHelpData>()._playerScore == Globals.enemiesQuantity)
        //{
        //    Globals.currentLevel += 1;
        //    if (Globals.currentLevel < 3)
        //    {
        //        Globals.playerPoints = 0;
        //        InstantiateEnemys.updateLevel();
        //        //SceneManager.LoadScene("Demo");
        //    }
        //    else if(Globals.currentLevel == 3) {
        //        StopAllCoroutines();
        //        SceneManager.LoadScene("GameOver");

        //    }
        //}
    }
    IEnumerator waitPartices()
    {
        yield return new WaitForSeconds(totalDuration/2);
        GetComponent<NavMeshAgent>().enabled = true;
        // _enemyBody.GetComponent<SpriteRenderer>().enabled = true;
        _letterCube.SetActive(true);
        
    }
}

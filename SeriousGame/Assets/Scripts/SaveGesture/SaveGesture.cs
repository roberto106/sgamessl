﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using ViveHandTracking;

public class SaveGesture : MonoBehaviour
{
    private List<string> rowData = new List<string>();
    Vector3 test;
    Vector3[] testArray = new Vector3[21];

    bool saving = false;
    void Update()
    {
        // if (Input.GetKeyDown(KeyCode.Space))
        //     saving = true;

        // if (saving)
        // {

        //     foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        //     {
        //         if (Input.GetKey(kcode)&& GestureProvider.RightHand != null) {
        //             Debug.Log("entra");
        //             //StartCoroutine(SaveInstance(testArray, test, kcode));
        //             StartCoroutine(SaveInstance(GestureProvider.RightHand.points, GestureProvider.RightHand.position, kcode));

        //         }
        //         //StartCoroutine(SaveInstance(GestureProvider.RightHand.points, GestureProvider.RightHand.position, kcode));
        //         //        addkey(GestureProvider.RightHand.points, GestureProvider.RightHand.position, kcode);
        //     }
        // }

        if ((Input.anyKeyDown && !saving)||Input.GetKeyDown(KeyCode.Escape))
        {

            foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(kcode)&& GestureProvider.RightHand != null) {
                    saving = true;
                    //StartCoroutine(SaveInstance(testArray, test, kcode));
                    StartCoroutine(SaveInstance(kcode));

                }
                //StartCoroutine(SaveInstance(GestureProvider.RightHand.points, GestureProvider.RightHand.position, kcode));
                //        addkey(GestureProvider.RightHand.points, GestureProvider.RightHand.position, kcode);
            }
        }
    }

    private void addkey(Vector3[] _vector, Vector3 pos, KeyCode key)
    {
        string str = pos.x.ToString("F4") + "\t" + pos.y.ToString("F4") + "\t" + pos.z.ToString("F4") + "\t";
        foreach (var point in _vector)
        {
            str += point.x.ToString("F4") + "\t" + point.y.ToString("F4") + "\t" + point.z.ToString("F4") + "\t";
        }
        switch (key)
        {
            //H|Q|R|X
            //7|17|18|24
            case KeyCode.A: str += "0"; break;
            case KeyCode.B: str += "1"; break;
            case KeyCode.C: str += "2"; break;
            case KeyCode.D: str += "3"; break;
            case KeyCode.E: str += "4"; break;
            case KeyCode.F: str += "5"; break;
            case KeyCode.G: str += "6"; break;
            case KeyCode.H: str += "7"; break;
            case KeyCode.I: str += "8"; break;
            case KeyCode.K: str += "9"; break;
            case KeyCode.L: str += "10"; break;
            case KeyCode.M: str += "11"; break;
            case KeyCode.N: str += "12"; break;
            case KeyCode.O: str += "13"; break;
            case KeyCode.P: str += "14"; break;
            case KeyCode.Q: str += "15"; break;
            case KeyCode.R: str += "16"; break;
            case KeyCode.S: str += "17"; break;
            case KeyCode.T: str += "18"; break;
            case KeyCode.U: str += "19"; break;
            case KeyCode.V: str += "20"; break;
            case KeyCode.W: str += "21"; break;
            case KeyCode.X: str += "22"; break;
            case KeyCode.Y: str += "23"; break;
            case KeyCode.Escape: SaveData(); StopCoroutine(SaveInstance(key)); break;
        }

        //str += "\n";
        rowData.Add(str);
        Debug.Log("string" + str);
        Debug.Log("string" + rowData.Count);

    }

    private void SaveData()
    {
        Debug.Log("saving");
        string filePath = Application.dataPath + "\\Dataset\\Data2" + DateTime.Now.Hour + "-" +
            DateTime.Now.Minute + "-" +
            DateTime.Now.Second + ".tsv";

        StreamWriter outStream = File.CreateText(filePath);
        foreach (var item in rowData)
        {
            outStream.WriteLine(item);
        }
        rowData.Clear();
        Debug.Log("Archivo guardado");
        outStream.Close();
    }
    IEnumerator SaveInstance(KeyCode key)
    {
        while (saving)
        {
            yield return new WaitForSecondsRealtime(0.01f);
            if (GestureProvider.RightHand != null)
            {
                addkey(GestureProvider.RightHand.points, GestureProvider.RightHand.position, key);   
            }
            if (key == KeyCode.Escape)
                saving = false;
        }
      


    }
    /*private void SaveData(Vector3[] _vector,KeyCode key)
    {
        
        string filePath = Application.dataPath+"\\Dataset\\Data"+key+".csv";
        string str="";
        string delimiter = "\t";
        string[] rowDataTemp = new string[3];
        if (GestureProvider.RightHand != null)
        {
            for (int q = 0; q < GestureProvider.RightHand.points.Length; q++)
            {
                rowDataTemp[0] = GestureProvider.RightHand.points[q].x.ToString("F");
                rowDataTemp[1] = GestureProvider.RightHand.points[q].y.ToString("F");
                rowDataTemp[2] = GestureProvider.RightHand.points[q].z.ToString("F");
            }
            rowData.Add(rowDataTemp);
            string[][] output = new string[rowData.Count][];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = rowData[i];
            }
            int length = output.GetLength(0);
            StringBuilder sb = new StringBuilder();

            for (int index = 0; index < length; index++)
                sb.AppendLine(string.Join(delimiter, output[index]));


            StreamWriter outStream = File.CreateText(filePath);
            outStream.WriteLine(sb);

            outStream.Close();
        }
        else
            Debug.Log("Mano no detectadad");
    }*/
}

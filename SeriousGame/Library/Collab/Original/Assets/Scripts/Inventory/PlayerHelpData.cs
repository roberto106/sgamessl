﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class PlayerHelpData : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _level;
    [SerializeField] private GameObject _helpPanel;
    [SerializeField] private Image _gestureImage;
    [SerializeField] private List<Sprite> _imageLevel;

    public TextMeshProUGUI _playerLife;
    public TextMeshProUGUI _enemiesLeft;

    bool procesing = false;

    private int _timerSeconds = 500;

    private void Start()
    {
        _playerLife.text = "Vida: " + Globals.playerLive.ToString();
        _enemiesLeft.text = "Puntaje: " + Globals.playerScore.ToString();
        SetImage();
        StartCoroutine(Timer());

    }

    private void SetImage()
    {
        _gestureImage.GetComponent<Image>().sprite = _imageLevel[Globals.levelRoom - 1];
    }

    void Update()
    {
        // if (Input.anyKeyDown)
        //    openHelpData();
        if ((Input.GetKeyDown(KeyCode.H)|| Input.GetKeyDown(KeyCode.Escape) || Input.GetButton("LViveConTrackpadPress") || Input.GetButton("RViveConTrackpadPress")) && !procesing)
            {
                procesing = true;
                StartCoroutine(openHelpData());
            }
    }
   
    IEnumerator Timer()
    {
        while(_timerSeconds > 0) { 

            yield return new WaitForSecondsRealtime(1);
            _timerSeconds -= 1;
            _level.text = (_timerSeconds).ToString();

        }
        if (Globals.levelUnlocked == Globals.levelRoom)
        {
            Globals.levelUnlocked += 1;
        }
        SceneManager.LoadScene("LevelSelector");

    }
    IEnumerator openHelpData()
    {
        if (_helpPanel.activeSelf)
            _helpPanel.SetActive(false);
        else
            _helpPanel.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        procesing = false;
    }
}

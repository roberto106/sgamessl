﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class rastreador : MonoBehaviour
{
    //public GameObject enemy;
    //public GameObject camara;
    public GameObject arrowR;//right arrow
    public GameObject arrowL;//left arrow
    public float newAngle = -300.0f;
    public bool esquina;
    public bool esquina2;
    public bool frente;
    public bool derecha;
    public bool izquierda;
    public bool atras;
    public float retraso;
    public float inicio;
    private Scene scene;
    //public InstantiateEnemys enemigos;

    //oficial
    /*void descomposicion(GameObject prueba, GameObject prueba2)
    {
        angle = Mathf.Atan((prueba.transform.position.y - prueba2.transform.position.y) / (prueba.transform.position.x -
            prueba2.transform.position.x));
        angle = angle * Mathf.Rad2Deg;
    }*/

    /*float MultiplicateModules(GameObject beta)
    {
        float modulos = Mathf.Sqrt(Mathf.Round(beta.transform.position.x * beta.transform.position.x +
            beta.transform.position.y * beta.transform.position.y + beta.transform.position.z * beta.transform.position.z));
        return modulos;
    }*/

    void Start()
    {
        arrowR.gameObject.SetActive(false);
        arrowL.gameObject.SetActive(false);
        retraso = 0.0f;
        inicio = 0.0f;
        scene = SceneManager.GetActiveScene();
        Globals.sceneName = scene.name;
        //StartCoroutine("changeAngle");
    }

    /*IEnumerator changeAngle()
    {

        
        for (int i = 0; i < 100; i++)
        {
            yield return new WaitForSeconds(10.0f);
            newAngle = Globals.angle;
        }
    }*/
    /*IEnumerator changeAngle()
    {
        newAngle = Globals.angle;
        for(int i = 0; i > -1; i++)
        {

            yield return new WaitForSeconds(0.2f);
            if (Globals.enemyvivo == true)
            {
                if (Globals.m_frente == true)
                {

                    if (newAngle >= -90 && newAngle <= 0)
                        arrowL.gameObject.SetActive(true);
                    if (newAngle >= 40 && newAngle <= 90)
                        arrowR.gameObject.SetActive(true);
                }
                else if (Globals.m_derecha == true)
                {
                    arrowL.gameObject.SetActive(false);
                    if (newAngle >= -90 && newAngle <= 90)
                        arrowR.gameObject.SetActive(true);
                }
                else if (Globals.m_izquierda == true)
                {
                    arrowR.gameObject.SetActive(false);
                    if (newAngle >= -90 && newAngle <= 90)
                        arrowL.gameObject.SetActive(true);
                }
                else if (Globals.m_atras == true)
                {
                    if (newAngle >= -70 && newAngle <= -30)
                        arrowR.gameObject.SetActive(true);
                    if (newAngle >= 40 && newAngle <= 60)
                        arrowL.gameObject.SetActive(true);
                }
                else if (Globals.m_esquina1 == true)
                {
                    arrowL.gameObject.SetActive(false);
                    if ((newAngle >= -90 && newAngle <= 0) || (newAngle >= 50 && newAngle <= 90))
                        arrowR.gameObject.SetActive(true);
                }
                else if (Globals.m_esquina2 == true)
                {
                    arrowR.gameObject.SetActive(false);
                    if ((newAngle >= 0 && newAngle <= 90) || (newAngle >= -180 && newAngle <= -90))
                        arrowL.gameObject.SetActive(true);
                }
            } else
            {

                arrowR.gameObject.SetActive(false);
                arrowL.gameObject.SetActive(false);
            }
        }
    }*/
    // Update is called once per frame
    void Update()
    {
        if(Globals.sceneName == "Demo")
        {
            retraso += Time.deltaTime;
            inicio += Time.deltaTime;
            //el 1.5f vendria a ser el tiempo que este visible la flecha,ya que despues de ese tiempo las flechas se volveran invisibles
            //y se volvera a validar si es que alguna debera aparecer
            if (retraso > 1.5f)
            {
                arrowR.gameObject.SetActive(false);
                arrowL.gameObject.SetActive(false);
                frente = Globals.m_frente;
                derecha = Globals.m_derecha;
                izquierda = Globals.m_izquierda;
                atras = Globals.m_atras;
                esquina = Globals.m_esquina1;
                esquina2 = Globals.m_esquina2;
                newAngle = Globals.angle;
                //Se debería usar (Globals.diffFactor + 2.25f), ya que ese tiempo se demora en generar una nueva pos
                //el Globals.diffFactor varia dependiendo la dificultad que se emplee
                //creo que se le puede sumar 2.25f o 2.50f, con 2.25f siento q la flecha aparece al mismo tiempo que se crea
                //el enemigo, y con 2.50f aparece ligeramente despues, creo que dependerá de como les guste más
                if (retraso > Globals.diffFactor + 2.25f) //use solo el valor de 2.5f para probar en demo, sin diffFactor
                {
                    if (Globals.m_frente == true)
                    {

                        if (newAngle >= -90 && newAngle <= 0)
                            arrowL.gameObject.SetActive(true);
                        if (newAngle >= 40 && newAngle <= 90)
                            arrowR.gameObject.SetActive(true);
                    }
                    else if (Globals.m_derecha == true)
                    {
                        arrowL.gameObject.SetActive(false);
                        if (newAngle >= -90 && newAngle <= 90)
                            arrowR.gameObject.SetActive(true);
                    }
                    else if (Globals.m_izquierda == true)
                    {
                        arrowR.gameObject.SetActive(false);
                        if (newAngle >= -90 && newAngle <= 90)
                            arrowL.gameObject.SetActive(true);
                    }
                    else if (Globals.m_atras == true)
                    {
                        if (newAngle >= -70 && newAngle <= -30)
                            arrowR.gameObject.SetActive(true);
                        if (newAngle >= 40 && newAngle <= 60)
                            arrowL.gameObject.SetActive(true);
                    }
                    else if (Globals.m_esquina1 == true)
                    {
                        arrowL.gameObject.SetActive(false);
                        if ((newAngle >= -90 && newAngle <= 0) || (newAngle >= 50 && newAngle <= 90))
                            arrowR.gameObject.SetActive(true);
                    }
                    else if (Globals.m_esquina2 == true)
                    {
                        arrowR.gameObject.SetActive(false);
                        if ((newAngle >= 0 && newAngle <= 90) || (newAngle >= -180 && newAngle <= -90))
                            arrowL.gameObject.SetActive(true);
                    }

                    retraso = 0.0f;
                }

            }

            //StartCoroutine("changeAngle");




            //pienso mostrar
            /*if (angle >= 40 || angle <= -20)
                arrowR.gameObject.SetActive(true);
            if ((angle >= -20 && angle <= 40) || angle >= 90)
                arrowL.gameObject.SetActive(true);*/
            //se ve mejor q antes
            //original por mientras
            /*if (newAngle >= -20 && newAngle <= 20)
                arrowR.gameObject.SetActive(true);
            if ((newAngle >= 70 && newAngle <= 90) || (newAngle <= -70 && newAngle >= -90))
                arrowL.gameObject.SetActive(true);*/


            //version final
            //if (newAngle >= -70 && newAngle <= -30)
            //    arrowR.gameObject.SetActive(true);
            //if (newAngle >= 40 && newAngle <= 60)
            //    arrowL.gameObject.SetActive(true);








            /////

            /*if (angle <= 40 && angle >= -90)
                arrowR.gameObject.SetActive(true);
            if (angle >= 40 && angle <= 90)
                arrowR.gameObject.SetActive(false);
            if ((angle >= 90 && angle <= 140) || (angle >= -270 && angle <= -220))
                arrowL.gameObject.SetActive(false);
            if ((angle >= 140 && angle <= 270) || (angle <= -90 && angle >= -220))
                arrowL.gameObject.SetActive(true);*/
            //}
        }
    }

}

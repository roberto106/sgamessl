﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class PlayerHelpData : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _timer;
    [SerializeField] private GameObject _helpPanel;
    [SerializeField] private Image _gestureImage;
    [SerializeField] private Image _secondImage;
    [SerializeField] private List<Sprite> _imageLevel;

    public TextMeshProUGUI _playerLife;
    public TextMeshProUGUI _score;

    bool procesing = false;

    public int _playerlifePoints;
    public int _playerScore;
    public int _timerSeconds;

    private void Start()
    {
        _playerLife.text = "Vida: " + _playerlifePoints; 
        _score.text = "Puntaje: " + _playerScore;
        SetImage();

        StartCoroutine(Timer());

    }

    private void SetImage()
    {
        
        if (!Globals.alter)
        {
            _secondImage.gameObject.SetActive(true);   
            _gestureImage.GetComponent<Image>().sprite = _imageLevel[Globals.levelRoom*2 - 2];

            _secondImage.GetComponent<Image>().sprite = _imageLevel[Globals.levelRoom * 2 - 1];
        }
        else
        {
            _gestureImage.GetComponent<Image>().sprite = _imageLevel[Globals.levelRoom - 1];
        }


    }

    void Update()
    {
        // if (Input.anyKeyDown)
        //    openHelpData();
        if ((Input.GetKeyDown(KeyCode.H)|| Input.GetKeyDown(KeyCode.Escape) || Input.GetButton("LViveConTrackpadPress") || Input.GetButton("RViveConTrackpadPress")) && !procesing)
            {
                procesing = true;
                StartCoroutine(openHelpData());
            }
    }
   
    IEnumerator Timer()
    {
        while(_timerSeconds > 0) { 

            yield return new WaitForSecondsRealtime(1);
            _timerSeconds -= 1;
            _timer.text = (_timerSeconds).ToString();

        }
        if (Globals.levelUnlocked == Globals.levelRoom)
        {
            Globals.levelUnlocked += 1;
        }
        SceneManager.LoadScene("LevelSelector");

    }
    IEnumerator openHelpData()
    {
        if (_helpPanel.activeSelf)
            _helpPanel.SetActive(false);
        else
            _helpPanel.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        procesing = false;
    }
}

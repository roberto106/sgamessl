﻿using System.Collections.Generic;
using UnityEngine;

public class Globals 
{
    public static List<string> letters=new List<string>();
    public static int bossesBeaten=0;
    public static int playerPoints=0;
    public static int playerScore=0;
    public static int currentLevel = 0;
    public static int enemiesQuantity = 10;
    public static float minDistEnemy = Mathf.Infinity;
    public static int letter=-1;
    public static float diffFactor = 2;
    public static bool disparo = false;
    //public static float rotationZ = 0f;
    public static float angle = 0;
    public static GameObject enemySelected;
    public static bool m_frente = false;
    public static bool m_derecha = false;
    public static bool m_izquierda = false;
    public static bool m_atras = false;
    public static bool m_esquina1 = false;
    public static bool m_esquina2 = false;
    public static int levelRoom = 1;
    public static int levelUnlocked = 1;
    public static bool finalBoss = false;
    public static bool bossAttacking = false;
    public static string sceneName = "";
    public static bool alter = true;
    public static int cantLevel = 8;

    //public static bool correctAnswer=false;
    //public static bool enemyvivo = false;
}

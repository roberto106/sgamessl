﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelector : MonoBehaviour
{
    public List<GameObject> rooms;

    public GameObject boss;
    public GameObject bossBullet;
    public GameObject profesor;

    private int numRoom;
    bool roomActives;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!Globals.finalBoss)
        {
            roomActives = true;
            bossBullet.SetActive(false);
            boss.SetActive(false);
            profesor.SetActive(true);
        } else {
            foreach (var room in rooms)
            {
                room.SetActive(false);
            }
            roomActives = false;
            boss.SetActive(true);
            bossBullet.SetActive(true);
            profesor.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (roomActives)
        {
            foreach (var room in rooms)
            {
                numRoom = int.Parse(room.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<TextMesh>().text);
                if(Globals.levelUnlocked > numRoom && room.transform.position.y < 1.5)
                {
                    room.transform.Translate(0f,0.8f,0f);
                    room.transform.GetChild(0).gameObject.SetActive(true);
                }
                else if(Globals.levelUnlocked == numRoom && room.transform.position.y < 1.5)
                {
                    room.transform.Translate(0f,1f*Time.deltaTime,0f);
                    room.transform.GetChild(0).gameObject.SetActive(true);
                }
                else if(Globals.levelUnlocked < numRoom)
                {
                    room.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
        }
    }
}

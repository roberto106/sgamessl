﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class newScene : MonoBehaviour
{

    public void ChargeScene(string sName)
    {
        SceneManager.LoadScene(sName);
    }
    public void ChargeDifficult(float t_difficult)
    {
        Globals.diffFactor = t_difficult;
    }
}

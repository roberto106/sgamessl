﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    [SerializeField] private GameObject _instructionsPanel;
    [SerializeField] private GameObject _mainMenu;
    public GameObject reticle;

    public void StartGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
    
    public void QuitAplication()
    {
        Application.Quit();
    }
    public void ShowInstructions()
    {
        _instructionsPanel.SetActive(true);
        _mainMenu.SetActive(false);
    }

    private void Update()
    {
        if (reticle != null)
        {
            var name = reticle.GetComponent<ReticlePoser>().hitTarget.name;
            switch(name)
            {
                case "Play":
                    if (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch"))
                    {
                        StartGame();
                    }
                    break;
                case "Options":
                    if (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch"))
                    {
                        ShowInstructions();
                    }
                    break;
                case "Exit":
                    if (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch"))
                    {
                        QuitAplication();
                    }
                    break;
            }
        }
        if(_instructionsPanel.activeSelf)
        {
            if (Input.GetButton("LViveConTriggerTouch") || Input.GetButton("RViveConTriggerTouch"))
            {
                _instructionsPanel.SetActive(false);
                _mainMenu.SetActive(true);
            }
        }
    }
}

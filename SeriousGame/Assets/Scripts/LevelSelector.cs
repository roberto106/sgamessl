﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
namespace Level { 
public class LevelSelector : MonoBehaviour
{
    [SerializeField] private GameObject _proffesor;

    public List<GameObject> rooms;
    public GameObject boss;
    public GameObject bossBullet;
    public GameObject levels;

    private int numRoom;
    bool roomActives;
    
    void Start()
    {
        if (!Globals.finalBoss)
        {
            roomActives = true;
            bossBullet.SetActive(false);
            boss.SetActive(false);
        } 
    }
   
    public void ActivateFinalLevel()
    {
            foreach (var room in rooms)
            {
                room.SetActive(false);
            }
            levels.SetActive(false);
            roomActives = false;
            boss.SetActive(true);
            bossBullet.SetActive(true);


        }
    void Update()
    {
        if (roomActives)
        {
            foreach (var room in rooms)
            {
                numRoom = int.Parse(room.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<TextMesh>().text);
                if(Globals.levelUnlocked > numRoom && room.transform.position.y < 1.5)
                {
                    room.transform.Translate(0f,0.8f,0f);
                    room.transform.GetChild(0).gameObject.SetActive(true);
                }
                else if(Globals.levelUnlocked == numRoom && room.transform.position.y < 1.5)
                {
                    room.transform.Translate(0f,1f*Time.deltaTime,0f);
                    room.transform.GetChild(0).gameObject.SetActive(true);
                }
                else if(Globals.levelUnlocked < numRoom)
                {
                    room.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
        }
    }
}
}